package lyrics.azlyrics;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Hello world!
 *
 */
public class App {
	public static String[] tags = {"<>","<br>","</br>","<i>","</i>","<div>","</div>","!-- Usage of azlyrics.com content by any third-party lyrics provider is prohibited by our licensing agreement. Sorry about that. -->"};
	public static void main(String[] args) {
		try {

			DefaultHttpClient httpClient = new DefaultHttpClient();
			String searchUrl = "https://search.azlyrics.com/search.php?q=Dove+Cameron+It's+Goin'+Down";
			HttpGet get = new HttpGet(searchUrl);
			HttpResponse response = httpClient.execute(get);
			String result = EntityUtils.toString(response.getEntity(), "UTF-8");
			String songUrl = parseHTML(result);
			System.out.println(songUrl);
			HttpGet getSong = new HttpGet(songUrl);
			HttpResponse songResponse = httpClient.execute(getSong);
			String lyrics = EntityUtils.toString(songResponse.getEntity(), "UTF-8");
			//System.out.println(parseLyrics(lyrics));
			//System.out.println(lyrics+"***********************************************\n");
			String r = getSubString(lyrics, "<!-- Usage of azlyrics.com content by any third-party lyrics provider is prohibited by our licensing agreement. Sorry about that. -->", "<!-- MxM banner -->");
			System.out.println(replaceHTML(sanitize(r)));
			//System.out.println(getSubString(lyrics, "<!-- Usage of azlyrics.com content by any third-party lyrics provider is prohibited by our licensing agreement. Sorry about that. -->", "<!-- MxM banner -->"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public static String parseHTML(String html) {
		String result = "";

		Document doc = Jsoup.parse(html, "UTF-8");
		boolean flag = true;
		Elements element = doc.getElementsByClass("panel");
		for (int i = 0; i < element.size(); i++) {
			String panel = element.get(i).child(0).child(0).ownText();
			if (panel.toLowerCase().contains("song")) {
				Elements tr = element.get(i).getElementsByTag("tr");
				for (int j = 0; j < tr.size(); j++) {

					if (tr.get(j).child(0).hasAttr("class")) {
						Elements a = tr.get(j).child(0).getElementsByTag("a");
						// System.out.println(a.get(0).attr("href"));
						result = a.get(0).attr("href");
						break;
					}
				}

			}
		}
		return result;
	}

	public static String parseLyrics(String html) {
		String result = "";

		Document doc = Jsoup.parse(html, "UTF-8");
		boolean flag = true;
		Elements element = doc.getElementsByClass("ringtone");
		Element e = element.get(0);
		while(flag){
			if(e.nextElementSibling().tag().equals("div")){
				System.out.println(e.nextElementSibling().tag());
				flag = false;
			}
			e = e.nextElementSibling();
		}
		System.out.println(element.get(0).nextElementSibling());

		return result;
	}
	
	public static String getSubString(String str, String first, String second){
		return str.substring(str.indexOf(first) + 1, str.indexOf(second));
	}
	
	public static String sanitize(String html){
		for(int i = 0; i < tags.length; i++){
			html = html.replaceAll(tags[i], "");
		}
		return html;
	}
	public static String replaceHTML(String html){
		html = html.replaceAll("&amp;", "&");
		html = html.replace("&quot;", "\"");
		return html;
	}
	
	public static String html2text(String html) {
	    return Jsoup.parse(html).text();
	}
}
