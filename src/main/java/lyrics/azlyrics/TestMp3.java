package lyrics.azlyrics;

import java.io.File;
import java.io.IOException;

import com.mpatric.mp3agic.ID3v1;
import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.NotSupportedException;
import com.mpatric.mp3agic.UnsupportedTagException;

public class TestMp3 {
	Lyrics lyrics;
	public void start() throws UnsupportedTagException, InvalidDataException, IOException, NotSupportedException, InterruptedException
	{
		lyrics = new Lyrics();
		File folder = new File("C:\\Users\\anarmun.m\\Downloads\\Billboard Hot 100 Singles Chart (26th Aug 2017) (Mp3 320kbps) [SSEC]\\Billboard Hot 100 Singles Chart (26th Aug 2017)");
		
		File[] listOfFiles = folder.listFiles();

	    for (int i = 0; i < listOfFiles.length; i++) {
	      if (listOfFiles[i].isFile()) {
	    	  String fileName = listOfFiles[i].getName();
	    	  if(fileName.endsWith(".mp3")){
	    		  getTags(folder+"\\"+fileName);
	    	  }
	      } else if (listOfFiles[i].isDirectory()) {
	        System.out.println("Directory " + listOfFiles[i].getName());
	      }
	    }
	}
	
	public void getTags(String fileName) throws NotSupportedException{
		Mp3File mp3file;
		ID3v2 id3v2Tag;
		try {
			mp3file = new Mp3File(fileName);
			if(mp3file.hasId3v2Tag())
			{
				id3v2Tag = mp3file.getId3v2Tag();
				id3v2Tag.setLyrics(lyrics.getLyrics(id3v2Tag.getArtist(), id3v2Tag.getTitle()));
				mp3file.save(id3v2Tag.getTitle()+".mp3");
				
				
				//System.out.println("Length of this mp3 is: " + mp3file.getLengthInSeconds() + " seconds");
				System.out.println(mp3file.getId3v2Tag().getArtist()+" - "+mp3file.getId3v2Tag().getTitle());
				//System.out.println("Has ID3v2 tag?: " + (mp3file.hasId3v2Tag() ? "YES" : "NO"));
				//System.out.println("Has custom tag?: " + (mp3file.hasCustomTag() ? "YES" : "NO"));
				
			}
			else if(mp3file.hasId3v1Tag()){
				System.out.println("*************************ID3V1TAG*************************");
			}
			
		} catch (UnsupportedTagException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
